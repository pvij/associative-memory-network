import argparse
import os
import numpy as np
from PIL import Image


def get_data(train_data_rel_path, img_dim):
    no_of_training_pts = len(os.listdir(train_data_rel_path))
    dim = np.prod(img_dim)
    X = np.zeros((dim, no_of_training_pts))
    for i, img in enumerate(os.listdir(train_data_rel_path)):
        img_path = os.path.join(train_data_rel_path, img)
        im = Image.open(img_path)
        X[:, i] = np.ravel(np.asarray(im))
    return X


def get_img_with_gaussian_noise(img_arr):
    mean = 0
    sd = 100
    noisy_img = img_arr + np.random.normal(mean, sd, img_arr.shape)
    return noisy_img


def retrieve_img(
    W,
    path_to_test_img,
    img_dim,
    add_noise_to_img,
    rel_path_to_save_orig_and_remembered_im,
):
    test_im = Image.open(path_to_test_img)
    test_im_arr = np.asarray(test_im)
    if add_noise_to_img:
        test_im_arr = get_img_with_gaussian_noise(test_im_arr)
    # While showing, doesn't need .convert('L'), maybe does that internally before
    # showing
    # Image.fromarray(test_im_arr).show()
    # .convert("L") clips out of range values and rounds decimal values
    # within range
    Image.fromarray(test_im_arr).convert("L").save(
        os.path.join(rel_path_to_save_orig_and_remembered_im, "original_img.png")
    )
    test_img_flattened = np.ravel(test_im_arr)
    output = np.matmul(W, test_img_flattened)
    reshaped_output_im = np.reshape(output, img_dim)
    im_from_output_array = Image.fromarray(reshaped_output_im)
    # saving original and recollected images as showing both of them is causing an
    # error, apparently PIL can write only one image in the /tmp folder at a time.
    # Both cannot be open at a time
    im_from_output_array.convert("L").save(
        os.path.join(rel_path_to_save_orig_and_remembered_im, "remembered_img.png")
    )


parser = argparse.ArgumentParser(description="Auto associative memory for images")
parser.add_argument(
    "train_data_rel_path",
    type=str,
    help="Relative path to the folder consisting of training images",
)
parser.add_argument(
    "path_to_test_img", type=str, help="Relative path to the image to be tested"
)
parser.add_argument("img_dim", type=int, nargs=2, help="Image dimension")
parser.add_argument(
    "--add_noise_to_img",
    dest="add_noise_to_img",
    action="store_true",
    help="Include option if you want to add noise to image",
)
parser.add_argument(
    "rel_path_to_save_orig_and_remembered_im",
    type=str,
    help="Relative path to the folder to save original and remembered images",
)
parser.set_defaults(add_noise_to_img=False)
args = parser.parse_args()
train_data_rel_path = args.train_data_rel_path
path_to_test_img = args.path_to_test_img
img_dim = args.img_dim
add_noise_to_img = args.add_noise_to_img
rel_path_to_save_orig_and_remembered_im = args.rel_path_to_save_orig_and_remembered_im
X = get_data(train_data_rel_path, img_dim)
Y = X.copy()
# W = np.matmul(Y, X.T)
W = np.matmul(Y, np.linalg.pinv(X))
retrieve_img(
    W,
    path_to_test_img,
    img_dim,
    add_noise_to_img,
    rel_path_to_save_orig_and_remembered_im,
)
