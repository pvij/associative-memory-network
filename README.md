<h3>Denoising images using auto associative memory network</h3>

This is a demonstration of an application of auto associative memory network. Images are fed into the network as training patterns and if a noisy version of an image (present in the training patterns) is presented to the network, it retrieves the original image.

The experiment has just been done for grey-scale images as of now and involves the following steps: -
1. The color images stored in a folder are converted to grey scale images of dimension of user's choice and stored in a separate folder.
2. The grey scale images are then fed to auto associative memory network. An image (from one of the training patterns) is entered as one of the inputs to the program. If the user adds the option to add noise to the input image, Gaussian noise is added to it. The training pattern closest to the input image is retrieved and stored in a folder along with the input image (the noisy image is stored if the option to add noise is present).
