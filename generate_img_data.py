from PIL import Image
import os
from tqdm import tqdm
import argparse


def get_usable_images(color_img_folder_path, usable_img_folder_path, height, width):
    for img in tqdm(os.listdir(color_img_folder_path)):
        img_path = os.path.join(color_img_folder_path, img)
        im = Image.open(img_path)
        grey_img = im.convert("L")
        resized_img = grey_img.resize((height, width), Image.ANTIALIAS)
        usable_img_dest_path = os.path.join(
            usable_img_folder_path, img.split(".")[0] + ".png"
        )
        resized_img.save(usable_img_dest_path)


def check_sizes(usable_img_folder_path, expected_size):
    for img in os.listdir(usable_img_folder_path):
        img_path = os.path.join(usable_img_folder_path, img)
        if Image.open(img_path).size != tuple(expected_size):
            print("Inconsistent size found - ", Image.open(img_path).size, " | ", img)


parser = argparse.ArgumentParser(
    description="Generating images for feeding to auto associative memory model"
)
parser.add_argument(
    "color_img_rel_path",
    type=str,
    help="Relative path to the folder consisting of color images to be converted to usable images",
)
parser.add_argument(
    "usable_img_rel_path",
    type=str,
    help="Relative path to the folder where you want the usable images to be stored",
)
parser.add_argument("img_dim", type=int, nargs=2, help="Image dimension")
args = parser.parse_args()
color_img_folder_path = args.color_img_rel_path
usable_img_folder_path = args.usable_img_rel_path
usable_img_size = args.img_dim
get_usable_images(
    color_img_folder_path,
    usable_img_folder_path,
    usable_img_size[0],
    usable_img_size[1],
)
check_sizes(usable_img_folder_path, usable_img_size)
